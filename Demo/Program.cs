﻿using ObjToDbSerialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo
{
    class Program
    {
        static void Main(string[] args)
        {            
            var customer = new Customer()
            {
                FirstName = "FirstName 1",
                LastName = "LastName 1",
                Address = new Address() { StreetName = "Stree 1" }
            };
            for (int i = 0; i < 2; i++)
            {
                customer.OtherAddresses.Add(new Address()
                {
                    StreetName = "Street 1_" + i.ToString()
                });
            }

            var dbserializer = new ObjToDbSerialization.DbSerizlizer("customer1.sdf");
            dbserializer.Serialize(customer);
            dbserializer.Save();

            var desCustomer = (Customer)dbserializer.Deserialize();
        }
    }

    class Customer : ObjToDbSerialization.IDbSerializable
    {
        public Customer()
        {
            OtherAddresses = new List<Address>();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Address Address { get; set; }
        public ICollection<Address> OtherAddresses { get; set; }

        public void Serialize(ObjToDbSerialization.ISerializationContext serializer)
        {
            serializer
                .SetInstance(this)
                .AddProperty(() => FirstName)
                .AddProperty(() => LastName)
                .AddProperty(() => Address)
                .AddProperty(() => OtherAddresses);
        }

        public void Deserialize(ObjToDbSerialization.IDeserilizationContext context)
        {
            context
                .SetProperty(() => FirstName)
                .SetProperty(() => LastName)
                .SetProperty(() => Address)
                .SetProperty(() => OtherAddresses);
        }
    }

    class Address : IDbSerializable
    {
        public string StreetName { get; set; }

        public void Serialize(ISerializationContext serializer)
        {
            serializer
                .SetInstance(this)
                .AddProperty(() => StreetName);
        }

        public void Deserialize(IDeserilizationContext context)
        {
            context
                .SetProperty(() => StreetName);
        }
    }
}


