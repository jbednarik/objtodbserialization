﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace ObjToDbSerialization
{
    class SerializationContext : ObjToDbSerialization.ISerializationContext
    {
        private string instanceType;
        private string name;

        private SerializationContext owner;
        private List<PropertyItem> properties = new List<PropertyItem>();
        private List<SerializationContext> instances = new List<SerializationContext>();

        internal SerializationContext()
        {
        }

        private SerializationContext(string name, SerializationContext owner)
        {
            this.name = name;
            this.owner = owner;
        }

        public ISerializationContext SetInstance(object instance)
        {
            this.instanceType = instance.GetType().FullName;
            return this;
        }

        public ISerializationContext AddProperty<T>(Expression<Func<T>> propertyExpression)
        {
            var propertyName = ((MemberExpression)propertyExpression.Body).Member.Name;
            var value = propertyExpression.Compile().Invoke();
            AddProperty(propertyName, value, typeof(T));
            return this;
        }

        public SerializationContext AddProperty(string propertyName, object value, Type propertyType)
        {

            if (value is IDbSerializable)
            {
                var idbSerializable = (IDbSerializable)value;
                var instance = new SerializationContext(propertyName, this);
                instances.Add(instance);
                idbSerializable.Serialize(instance);
            }
            else if (!(value is string) && value is IEnumerable)
            {
                var instance = new SerializationContext(propertyName, this);
                instance.instanceType = "Array";
                instances.Add(instance);
                var values = (IEnumerable)value;

                foreach (var item in values)
                {
                    instance.AddArrayItem(item);
                }
            }
            else
            {
                var type = propertyType;

                var item = new PropertyItem
                {
                    Name = propertyName,
                    Type = type,
                    Value = value.ToString(),
                    Owner = instanceType,
                };

                properties.Add(item);
            }

            return this;
        }

        private List<PropertyItem> arrProperties = new List<PropertyItem>();
        private List<SerializationContext> arrInstances = new List<SerializationContext>();

        private void AddArrayItem(object item)
        {
            if (item is IDbSerializable)
            {
                var idbSerializable = (IDbSerializable)item;
                var instance = new SerializationContext(null, this);
                arrInstances.Add(instance);
                idbSerializable.Serialize(instance);
            }
            else
            {

                var propertyItem = new PropertyItem
                {
                    Type = item.GetType(),
                    Value = item.ToString(),
                    Owner = instanceType,
                };

                arrProperties.Add(propertyItem);
            }
        }

        internal void Save(ObjectSerializationDbContext db)
        {
            var inst = new Instance()
            {
                TypeFullName = instanceType
            };

            db.Instances.Add(inst);

            foreach (var item in properties)
            {
                inst.Children.Add(new Instance
                {
                    Name = item.Name,
                    TypeFullName = item.Type.FullName,
                    Value = item.Value,
                    Owner = inst,
                });
            }

            foreach (var item in instances)
            {
                item.Save(db, inst);
            }
        }

        private void Save(ObjectSerializationDbContext db, Instance owner)
        {
            var inst = new Instance()
            {
                Name = name,
                Owner = owner,
                TypeFullName = instanceType,
            };

            owner.Children.Add(inst);
            db.Instances.Add(inst);

            foreach (var item in properties)
            {
                inst.Children.Add(new Instance
                {
                    Name = item.Name,
                    TypeFullName = item.Type.FullName,
                    Value = item.Value,
                    Owner = inst,
                });
            }

            foreach (var item in instances)
            {
                item.Save(db, inst);
            }

            foreach (var item in arrInstances)
            {
                item.Save(db, inst);
            }
        }

        struct PropertyItem
        {
            public string Name;
            public Type Type;
            public string Value;
            public string Owner { get; set; }
        }
    }
}
