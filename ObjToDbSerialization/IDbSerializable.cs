﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjToDbSerialization
{
    public interface IDbSerializable
    {
        void Serialize(ISerializationContext serializer);
        void Deserialize(IDeserilizationContext context);
    }
}
