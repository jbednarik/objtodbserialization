﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace ObjToDbSerialization
{
    class DeserilizationContext : ObjToDbSerialization.IDeserilizationContext
    {
        private Instance instance;

        internal DeserilizationContext(Instance instance)
        {
            this.instance = instance;
        }

        public IDeserilizationContext SetProperty<T>(Expression<Func<T>> propertyExpression)
        {
            //http://msmvps.com/blogs/paulomorgado/archive/2011/01/12/creating-property-set-expression-trees-in-a-developer-friendly-way.aspx

            var member = ((MemberExpression)propertyExpression.Body).Member;
            var propInfo = member as PropertyInfo;
            if (propInfo.PropertyType != typeof(string) && typeof(IEnumerable).IsAssignableFrom(propInfo.PropertyType))
            {
                var value = propertyExpression.Compile().Invoke() as IEnumerable;
                var addMethod = value.GetType().GetMethod("Add");

                foreach (var item in Get(member.Name).Children)
                {
                    var propertuInstance = item;

                    if (propertuInstance.Value != null)
                    {
                        addMethod.Invoke(value, new[] { propertuInstance.Value });
                    }
                    else
                    {
                        var type = DbSerizlizer.FindType(propertuInstance.TypeFullName);
                        var idbSerializableItem = (IDbSerializable)Activator.CreateInstance(type);
                        idbSerializableItem.Deserialize(new DeserilizationContext(propertuInstance));
                        addMethod.Invoke(value, new[] { idbSerializableItem });
                    }
                }
            }
            else
            {

                var propName = ((MemberExpression)propertyExpression.Body).Member.Name;
                var propertuInstance = Get(propName);
                var type = DbSerizlizer.FindType(propertuInstance.TypeFullName);
                var valueParameterExpression = Expression.Parameter(type);

                var lambdaExpr = Expression.Lambda(
                    body: Expression.Assign(
                        left: propertyExpression.Body,
                        right: valueParameterExpression),
                    parameters: new[] {
                    valueParameterExpression 
                });

                if (propertuInstance.Value != null)
                {
                    ((Func<string, T>)lambdaExpr.Compile()).Invoke(propertuInstance.Value);
                }
                else
                {
                    var value = (IDbSerializable)Activator.CreateInstance(type);
                    value.Deserialize(new DeserilizationContext(propertuInstance));

                    ((Func<T, T>)lambdaExpr.Compile()).Invoke((T)value);
                }
            }
            return this;
        }

        private Instance Get(string propName)
        {
            var result = instance.Children.First(x => x.Name.Equals(propName, StringComparison.Ordinal));
            return result;
        }
    }

}
