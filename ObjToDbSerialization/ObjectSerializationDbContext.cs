﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace ObjToDbSerialization
{
    class ObjectSerializationDbContext : System.Data.Entity.DbContext
    {
        private const string ProviderInvariantName = "System.Data.SqlServerCe.4.0";
        private const string DatbaseConnectionStringFormat = "Datasource={0}";

        static ObjectSerializationDbContext()
        {
            Database.DefaultConnectionFactory = new SqlCeConnectionFactory(ProviderInvariantName);
        }

        public ObjectSerializationDbContext()
            : this(null)
        {
        }

        public ObjectSerializationDbContext(string databaseFilename)
            : this(ConnectionString1(databaseFilename), true)
        {
        }

        public ObjectSerializationDbContext(string databaseFilename, bool createNew)
            : base(ConnectionString1(databaseFilename))
        {
            if (createNew)
                Database.SetInitializer<ObjectSerializationDbContext>(new DropCreateDatabaseAlways<ObjectSerializationDbContext>());
            else
                Database.SetInitializer<ObjectSerializationDbContext>(new CreateDatabaseIfNotExists<ObjectSerializationDbContext>());
        }

        public DbSet<Instance> Instances { get; set; }

        protected override void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        private static string ConnectionString1(string filename)
        {
            return string.Format(DatbaseConnectionStringFormat, filename ?? string.Format("{0}.{1}", typeof(ObjectSerializationDbContext).Name, "sdf"));
        }
    }
}
