﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjToDbSerialization
{
    public class DbSerizlizer
    {
        ObjectSerializationDbContext db;

        private List<SerializationContext> instances = new List<SerializationContext>();
        private static ConcurrentDictionary<string, ConcurrentBag<Type>> cache;

        public DbSerizlizer()
            : this(new ObjectSerializationDbContext())
        {
        }

        public DbSerizlizer(string databaseFilename)
            : this(new ObjectSerializationDbContext(databaseFilename))
        {
        }

        private DbSerizlizer(ObjectSerializationDbContext o)
        {
            this.db = o;
        }

        public DbSerizlizer Serialize(IDbSerializable instance)
        {
            var type = instance.GetType();

            var typeName = type.Name;
            var fullName = type.FullName;

            var inst = new SerializationContext();
            instance.Serialize(inst);
            instances.Add(inst);
            return this;
        }

        public object Deserialize()
        {
            var instance = db.Instances.First();
            var type = FindType(instance.TypeFullName);
            var c = (IDbSerializable)Activator.CreateInstance(type);
            c.Deserialize(new DeserilizationContext(instance));
            return c;
        }

        internal static Type FindType(string typeFullname)
        {
            if (cache == null)
            {
                CreateCache();
            }

            return cache[typeFullname].First();
        }

        private static void CreateCache()
        {
            var type = (from asm in AppDomain.CurrentDomain.GetAssemblies()
                        from t in asm.GetTypes()
                        where t.IsClass
                        where !t.IsAbstract
                        select t);
            cache = new ConcurrentDictionary<string, ConcurrentBag<Type>>();
            foreach (var item in type)
            {
                var lazyBag = new Lazy<ConcurrentBag<Type>>(() => { return new ConcurrentBag<Type>(new[] { item }); });
                cache.AddOrUpdate(item.FullName, lazyBag.Value, (key, bag) =>
                {
                    bag.Add(item);
                    return bag;
                });
            }
        }

        public void Save()
        {
            foreach (var item in instances)
            {
                item.Save(db);
            }
            var result = db.SaveChanges();
        }
    }
}
