﻿using System;
using System.Linq.Expressions;
namespace ObjToDbSerialization
{
    public interface ISerializationContext
    {
        ISerializationContext AddProperty<T>(Expression<Func<T>> propertyExpression);
        ISerializationContext SetInstance(object instance);
    }
}
