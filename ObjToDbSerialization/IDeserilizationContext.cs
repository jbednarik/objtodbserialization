﻿using System;
using System.Linq.Expressions;
namespace ObjToDbSerialization
{
    public interface IDeserilizationContext
    {
        IDeserilizationContext SetProperty<T>(Expression<Func<T>> propertyExpression);
    }
}
