﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace ObjToDbSerialization
{
    class Instance
    {
        [DebuggerStepThrough]
        public Instance()
        {
            Children = new List<Instance>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string TypeFullName { get; set; }
        public string Value { get; set; }
        public ICollection<Instance> Children { get; set; }
        public Instance Owner { get; set; }
    }
}
